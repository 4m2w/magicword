package bit.team42.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import bit.team42.domain.MemberVO;
import bit.team42.service.StatsService;

@Controller
@RequestMapping("/stats/*")
public class StatsController {
	
	@Inject
	StatsService statsService;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public void registerGET(Integer clevel, Model model) throws Exception {
		
		model.addAttribute("visitToday",statsService.visitTodayCount());
		model.addAttribute("visitTotal",statsService.visitTotalCount());
		model.addAttribute("memberCount",statsService.memberTotalCount());
		model.addAttribute("childCount",statsService.childTotalCount());
		model.addAttribute("markerCount",statsService.markerTotalCount());
		model.addAttribute("hanzaCount",statsService.hanzaTotalCount());



	}
	

}
