package bit.team42.persistence;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;


@Repository
public class StatsDAO {
	
	@Inject
	private SqlSession session;
	
	private static final String namespace = "bit.team42.visitMapper";

	public void visitInsert(){
		session.insert(namespace+".setVisitTotalCount");
	}
	
	public int visitTotalCount(){
		return session.selectOne(namespace+".getVisitTotalCount");
	}
	
	public int visitTodayCount(){
		return session.selectOne(namespace+".getVisitTodayCount");
	}
	
	public int memberTotalCount(){
		return session.selectOne(namespace+".memberTotalCount");
	}
	
	public int childTotalCount(){
		return session.selectOne(namespace+".childTotalCount");
	}
	
	public int markerTotalCount(){
		return session.selectOne(namespace+".markerTotalCount");
	}
	
	public int hanzaTotalCount(){
		return session.selectOne(namespace+".hanzaTotalCount");
	}
	


}
