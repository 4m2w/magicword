package bit.team42.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import bit.team42.persistence.StatsDAO;

@Service
public class StatsService {
	
	@Inject
	private StatsDAO dao;
	
	public void visitInsert() throws Exception{
		dao.visitInsert();
	}
	
	public int visitTotalCount() throws Exception{
		return dao.visitTotalCount();
	}
	
	public int visitTodayCount() throws Exception{
		return dao.visitTodayCount();
	}
	
	public int memberTotalCount() throws Exception{
		return dao.memberTotalCount();
	}
	
	public int childTotalCount() throws Exception{
		return dao.childTotalCount();
	}
	
	public int markerTotalCount() throws Exception{
		return dao.markerTotalCount();
	}
	
	public int hanzaTotalCount() throws Exception{
		return dao.hanzaTotalCount();
	}
	


}
